﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SegasiumCustomControl.WpfTrajectoryControl
{
    public class TrajectoryViewerModel
    {
        public event EventHandler RedrawViewer = delegate { };
        public void Redraw()
        {
            RedrawViewer(this, null);
        }
        /// <summary>
        /// Коллекция углов в радианах
        /// </summary>
        public List<double> Angles { get; private set; }

        public TrajectoryViewerModel()
        {
            Angles = new List<double>();
        }
    }
}
