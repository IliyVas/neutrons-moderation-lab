﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SegasiumCustomControl.WpfTrajectoryControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class TrajectoryViewer : UserControl
    {
        public static readonly DependencyProperty TrjViewModelProperty =
            DependencyProperty.Register("TrjViewModel", typeof(TrajectoryViewerModel), typeof(TrajectoryViewer), new PropertyMetadata(null, ContolModelProperty_PropertyChanged));
        public TrajectoryViewerModel TrjViewModel
        {
            get { return (TrajectoryViewerModel)this.GetValue(TrjViewModelProperty); }
            set { this.SetValue(TrjViewModelProperty, value); }
        }
        private static void ContolModelProperty_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((TrajectoryViewer)obj).BindModel();
        }

        public void BindModel()
        {
            lock (this.modelLock)
            {
                if (this.currentModel != null)
                {
                    this.currentModel = null;
                }
                if (this.TrjViewModel != null)
                {
                    this.currentModel = TrjViewModel;
                    this.currentModel.RedrawViewer += ControlModel_RedrawViewer;
                }
            }
            this.RedrawViewer();
        }

        private readonly object modelLock = new object();
        private TrajectoryViewerModel currentModel;

        void ControlModel_RedrawViewer(object sender, EventArgs e)
        {
            RedrawViewer();
        }

        private List<Line> Trajectores { get; set; }

        private void RedrawViewer()
        {
            Trajectores.ForEach(line => ViewerContainer.Children.Remove(line));
            Trajectores.Clear();

            var clusterizator = new ClusterCreator();
            var resClusterization = clusterizator.Clusterize(currentModel.Angles, 0.03d);

            var edgeX = 150d;
            var edgeY = 150d;
            var nearEdge = Math.Min(edgeX, edgeY);
            
            // Выводим кластеры
            resClusterization.ForEach(x =>
            {
                var angle = x.Angle;
                var xCoordLine = 0d;
                var yCoordLine = 0d;
                if (Math.Abs(x.Angle - Math.PI / 2) < 0.001)
                {
                    if (x.Angle < Math.PI)
                    {
                        xCoordLine = 0;
                        yCoordLine = edgeY;
                    }
                    else
                    {
                        xCoordLine = 0;
                        yCoordLine = -edgeY;
                    }
                }
                else
                {
                    xCoordLine = 500 * Math.Cos(x.Angle);
                    yCoordLine = 500 * Math.Sin(x.Angle);

                    if (Math.Abs(xCoordLine) > edgeX)
                    {
                        xCoordLine = Math.Sign(xCoordLine) * edgeX;
                        yCoordLine = Math.Tan(x.Angle) * xCoordLine;
                    }

                    if (Math.Abs(yCoordLine) > edgeY)
                    {
                        yCoordLine = Math.Sign(yCoordLine) * edgeY;
                        xCoordLine = yCoordLine / Math.Tan(x.Angle);
                    }
                }
                var line = new Line();
                line.X1 = 150;
                line.Y1 = 150;
                line.X2 = edgeX + xCoordLine;
                line.Y2 = edgeY - yCoordLine;
                line.Stroke = Brushes.Black;
                line.StrokeThickness = 1;
                line.StrokeDashArray = new DoubleCollection(new double[] { 8, 3 });
                line.Cursor = Cursors.Help;
                line.Style = (Style)ViewerContainer.FindResource("HighlightLine");

                var gridToolTipCont = new Grid();
                var textBlock = new TextBlock();
                textBlock.MaxHeight = 300;
                x.Cluster.OrderBy(y => y.Num).ToList().ForEach(i =>
                {
                    textBlock.Inlines.Add(new Run(String.Format("Траектория столкновения №{0}", i.Num)));
                    textBlock.Inlines.Add(new LineBreak());
                    textBlock.Inlines.Add(new Run(String.Format("Угол: {0:F2}", 180 * i.Angle / Math.PI)));
                    textBlock.Inlines.Add(new LineBreak());
                });

                gridToolTipCont.Children.Add(textBlock);
                var toolTip = new ToolTip();
                toolTip.Content = gridToolTipCont;

                line.ToolTip = toolTip;
                ViewerContainer.Children.Insert(0, line);
                Trajectores.Add(line);
            });
        }

        public TrajectoryViewer()
        {
            InitializeComponent();
            Trajectores = new List<Line>();
        }
    }
}
