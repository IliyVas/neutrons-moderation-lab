﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SegasiumCustomControl
{
    public struct AngleInfo
    {
        public int Num{ get; private set; }
        public double Angle { get; private set; }
        public AngleInfo(int num, double angle)
            : this()
        {
            Num = num;
            Angle = angle;
        }
    }

    public struct ClusterOutput{
        public double Angle{ get; private set; }
        public ReadOnlyCollection<AngleInfo> Cluster { get; private set; }
        public ClusterOutput(double angle, List<AngleInfo> cluster)
            : this()
        {
            Angle = angle;
            Cluster = cluster.AsReadOnly();
        }
    }

    public class ClusterCreator
    {
        private ConcurrentDictionary<int, double> AngleSpis;

        public ClusterCreator()
        {
            AngleSpis = new ConcurrentDictionary<int, double>();
        }

        /// <summary>
        /// Кластеризует заданный список точек используя заданный радиус кластера.
        /// </summary>
        /// <param name="inputAngles">Список углов в порядке соударения</param>
        /// <param name="clusterR">Максимальное расстояние от центра кластера до самой далекой точки из этого кластера</param>
        /// <returns>Возвращает полученные кластеры</returns>
        public List<ClusterOutput> Clusterize(List<double> inputAngles, double clusterR)
        {
            if (inputAngles.Count == 0)
            {
                return new List<ClusterOutput>();
            }

            // Шаг 0. Нумеруем углы
            AngleSpis.Clear();
            int num = 0;
            inputAngles.ForEach(x => AngleSpis.TryAdd(++num, x));
            
            // Шаг 1. Создаем кластеры
            var clusters = new ConcurrentDictionary<int, List<int>>();
            Parallel.ForEach(AngleSpis, x =>
            {
                var newCluster = new List<int>();
                foreach (var angleItem in AngleSpis)
                {
                    if (Math.Abs(x.Value - angleItem.Value) <= clusterR)
                    {
                        newCluster.Add(angleItem.Key);
                    }
                }
                clusters.TryAdd(x.Key, newCluster);
            });

            // Шаг 2. Убираем единичные кластеры (состоящие из одной точки)
            var endClusters = new Dictionary<int, List<int>>();
            endClusters = clusters.AsParallel().Where(x => x.Value.Count == 1).ToDictionary(x => x.Key, x => x.Value);

            endClusters.AsParallel().ForAll(x => {
                List<int> output;
                clusters.TryRemove(x.Key, out output);
            });

            // Шаг 3. Считаем максимальные кластеры и добавляем из в конечные, удаляя точки из него со всех остальных кластеров
            Func<int> getMaxCluster = () => clusters.AsParallel().Aggregate<KeyValuePair<int,List<int>>, Tuple<int, int>, int>(
                Tuple.Create(-1, -1),
                (subMax, x) => subMax.Item2 < x.Value.Count ? Tuple.Create(x.Key, x.Value.Count) : subMax,
                (totalMax, subMax) => totalMax.Item2 < subMax.Item2 ? subMax : totalMax,
                (totalMax) => totalMax.Item2 <= 1 ? -1 : totalMax.Item1
            );
            int maxCluster;
            while ((maxCluster = getMaxCluster()) != -1){
                List<int> output;
                if (!clusters.TryGetValue(maxCluster, out output))
                {
                    throw new NotImplementedException();
                }
                endClusters.Add(maxCluster, output);

                Parallel.ForEach(clusters, x =>
                {
                    if (output.Contains(x.Key))
                    {
                        List<int> outputCl;
                         if (!clusters.TryRemove(x.Key, out outputCl)){
                             throw new InvalidOperationException();
                         }
                    }
                    else
                    {
                        var newCl = x.Value.Except(output).ToList();
                        if (newCl.Count != x.Value.Count)
                        {
                            clusters.AddOrUpdate(x.Key, newCl, (k, oldVal) => newCl);
                        }
                    }
                });
            }

            // Шаг 4. Добавляем оставшиеся единичные точки
            if (clusters.Count > 0)
            {
                foreach (var clusterItem in clusters)
                {
                    endClusters.Add(clusterItem.Key, clusterItem.Value);
                }
            }

            ConcurrentBag<ClusterOutput> resListClusters = new ConcurrentBag<ClusterOutput>();
            endClusters.AsParallel().ForAll(x =>
            {
                var outCluster = x.Value.Join(AngleSpis, i => i, j => j.Key, (key, findEl) => new AngleInfo(key, findEl.Value)).ToList();
                resListClusters.Add(new ClusterOutput(AngleSpis.FirstOrDefault(i => i.Key == x.Key).Value, outCluster));
            });

            return resListClusters.ToList();
        }
    }
}
