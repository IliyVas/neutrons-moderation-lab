﻿using NeutronsScattering.View;
using NeutronsScattering.ViewModel;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace NeutronsScattering
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public IMainWindowViewModel model;

        public MainWindow()
        {
            InitializeComponent();

            var navService = new FrameNavigationService(BrowserProg);
            BrowserProg.Source = new Uri("../View/IntroductionPage.xaml", UriKind.Relative);
            model = new MainWindowViewModel(navService);
            DataContext = model;
            ViewModelsVariables.MainVM = model;
        }

        private void MainGrid_SizeChanged(object sender, EventArgs e)
        {
            CalculateScale();
        }

        private void CalculateScale()
        {
            double yScale = ActualHeight / 400d;
            double xScale = ActualWidth / 800d;
            double value = Math.Max(Math.Min(Math.Min(xScale, yScale), 1.4), 1);
            model.ScaleValue = value;
        }
    }
}
