﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NeutronsScattering.View
{
    /// <summary>
    /// Логика взаимодействия для Lab1Print.xaml
    /// </summary>
    public partial class Lab1Print : Page
    {
        public Lab1Print()
        {
            InitializeComponent();
            NeutronsScattering.ViewModel.ViewModelsVariables.MainVM.BeforeNavigating += MainVM_BeforeNavigating;
        }

        void MainVM_BeforeNavigating(object sender, ViewModel.BegoreNavigateArgs e)
        {
            if (e.CurrentPageKey == "LabPrint")
            {
                BindingOperations.ClearBinding(flwDocReader, FlowDocumentReader.DocumentProperty);
                NeutronsScattering.ViewModel.ViewModelsVariables.MainVM.BeforeNavigating -= MainVM_BeforeNavigating;
            }
        }
    }
}
