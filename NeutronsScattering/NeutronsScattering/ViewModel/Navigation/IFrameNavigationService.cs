﻿using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeutronsScattering.ViewModel
{
    public interface IFrameNavigationService : INavigationService
    {
        void Configure(string pageKey, Uri pageType);
    }
}
