﻿using NeutronsScattering.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeutronsScattering.ViewModel.Navigation
{
    public class ElementInfo
    {
        public IChemElement UsingElement { get; private set; } 
        public double StartEnergy { get; private set; }
        public double FinalEnergy { get; private set; }
        public IEnergyUnit InitEnergyUnit { get; private set; }
        public IEnergyUnit FinalEnergyUnit { get; private set; }

        public ElementInfo(Lab1PracticeViewModel _model)
        {
            UsingElement = _model.UsingElement;
            StartEnergy = _model.StartEnergy;
            InitEnergyUnit = _model.InitEnergyUnit;
            FinalEnergy = _model.FinalEnergy;
            FinalEnergyUnit = _model.FinalEnergyUnit;
        }
    }
}
