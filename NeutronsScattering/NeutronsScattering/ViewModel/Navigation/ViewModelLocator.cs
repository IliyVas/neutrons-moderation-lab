/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:NeutronsScattering"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;

namespace NeutronsScattering.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}
            var navService = ViewModelsVariables.MainVM.NavService;
            navService.Configure("Intro", new Uri("../View/IntroductionPage.xaml", UriKind.Relative));
            navService.Configure("LabPart1", new Uri("../View/Lab1PracticePage.xaml", UriKind.Relative));
            navService.Configure("LabPart2", new Uri("../View/Lab1NeuronScatteringSpectrum.xaml", UriKind.Relative));
            navService.Configure("LabPrint", new Uri("../View/Lab1Print.xaml", UriKind.Relative));

            SimpleIoc.Default.Register<IFrameNavigationService>(() => navService);

            SimpleIoc.Default.Register<IntroductionViewModel>(() => new IntroductionViewModel(navService));
            SimpleIoc.Default.Register<Lab1PracticeViewModel>(() => new Lab1PracticeViewModel(navService));
            SimpleIoc.Default.Register<Lab1NSSViewModel>(() => new Lab1NSSViewModel(navService), true);
            SimpleIoc.Default.Register<Lab1PrintViewModel>(() => new Lab1PrintViewModel(navService), true);

            navService.NavigateTo("Intro");
        }

        public IntroductionViewModel Introduction
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IntroductionViewModel>();
            }
        }
        public Lab1PracticeViewModel Lab1Practice
        {
            get
            {
                return ServiceLocator.Current.GetInstance<Lab1PracticeViewModel>();
            }
        }
        public Lab1NSSViewModel Lab1NSS
        {
            get
            {
                return ServiceLocator.Current.GetInstance<Lab1NSSViewModel>();
            }
        }

        public Lab1PrintViewModel Lab1Print
        {
            get
            {
                return ServiceLocator.Current.GetInstance<Lab1PrintViewModel>();
            }
        }
        
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}