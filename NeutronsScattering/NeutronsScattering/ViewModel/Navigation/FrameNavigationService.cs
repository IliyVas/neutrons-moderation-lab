﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace NeutronsScattering.ViewModel
{
    public class FrameNavigationService : PropertyChangedBase, IFrameNavigationService
    {
        private readonly Dictionary<string, Uri> _pageKeys;
        private string _currentPageKey;
        private Frame _frame;

        public FrameNavigationService(Frame frame)
        {
            _pageKeys = new Dictionary<string, Uri>();
            _frame = frame;
            //_frame = GetDescendantFromName(App.Current.MainWindow, "BrowserProg") as Frame;
        }

        public string CurrentPageKey
        {
            get { return _currentPageKey; }
            private set { SetField(ref _currentPageKey, value, "CurrentPageKey"); }
        }

        public void GoBack()
        {
            throw new NotImplementedException();
        }

        public virtual void NavigateTo(string pageKey, object parameter)
        {
            lock (_pageKeys)
            {
                if (!_pageKeys.ContainsKey(pageKey))
                {
                    throw new KeyNotFoundException("Страница с таким ключем не обнаружена");
                }

                if (pageKey != _currentPageKey)
                {
                    if (ViewModelsVariables.MainVM.BeforeNavigate(CurrentPageKey, pageKey))
                    {
                        _frame.Navigate(_pageKeys[pageKey]);
                        CurrentPageKey = pageKey;
                    }
                }
            }
        }

        public void NavigateTo(string pageKey)
        {
            NavigateTo(pageKey, null);
        }

        public void Configure(string pageKey, Uri pageType)
        {
            lock (_pageKeys)
            {
                if (_pageKeys.ContainsKey(pageKey))
                {
                    _pageKeys[pageKey] = pageType;
                }else{
                    _pageKeys.Add(pageKey, pageType);
                }
            }
        }

        //private static FrameworkElement GetDescendantFromName(DependencyObject parent, string name)
        //{
        //    var count = VisualTreeHelper.GetChildrenCount(parent);

        //    if (count < 1)
        //    {
        //        return null;
        //    }

        //    for (var i = 0; i < count; i++)
        //    {
        //        var frameworkElement = VisualTreeHelper.GetChild(parent, i) as FrameworkElement;
        //        if (frameworkElement != null)
        //        {
        //            if (frameworkElement.Name == name)
        //            {
        //                return frameworkElement;
        //            }

        //            frameworkElement = GetDescendantFromName(frameworkElement, name);
        //            if (frameworkElement != null)
        //            {
        //                return frameworkElement;
        //            }
        //        }
        //    }
        //    return null;
        //}
    }
}
