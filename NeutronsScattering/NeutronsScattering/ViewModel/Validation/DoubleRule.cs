﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Input;

namespace NeutronsScattering.ViewModel.Validation
{
    public class DoubleRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo culture)
        {
            double result = 0;
            if (!Double.TryParse((string)value, NumberStyles.Any, InputLanguageManager.Current.CurrentInputLanguage, out result))
            {
                return new ValidationResult(false, "Недопустимые символы.");
            }

            if (result < 0){
                return new ValidationResult(false, "Число должно быть положительным");
            }

            return new ValidationResult(true, null);
        }
    }
}
