﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;

namespace NeutronsScattering.ViewModel.Validation
{
    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return ((double)value).ToString("0.##", culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return value = Double.Parse(value.ToString().Replace(",", "."), NumberStyles.Any, new CultureInfo("en-US"));
        }
    }
}
