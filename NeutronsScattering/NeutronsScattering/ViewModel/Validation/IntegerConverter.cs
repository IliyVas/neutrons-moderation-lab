﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;

namespace NeutronsScattering.ViewModel.Validation
{
    [ValueConversion(typeof(int), typeof(string))]
    public class IntegerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return ((int)value).ToString(culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return value = int.Parse(value.ToString(), NumberStyles.Any, InputLanguageManager.Current.CurrentInputLanguage);
        }
    }
}
