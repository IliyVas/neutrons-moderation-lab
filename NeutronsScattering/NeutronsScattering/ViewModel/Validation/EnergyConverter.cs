﻿using NeutronsScattering.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace NeutronsScattering.ViewModel.Validation
{
    [ValueConversion(typeof(double), typeof(string))]
    public class EnergyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (DBNull.Value.Equals(value))
            {
                return "";
            }

            var enrgy = (double)value;
            return EnergyUnits.GetStringWithEnergyUnits(enrgy);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
