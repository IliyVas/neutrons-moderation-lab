﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using NeutronsScattering.Model;
using NeutronsScattering.ViewModel.Navigation;
using OxyPlot;
using OxyPlot.Axes;
using SegasiumCustomControl.WpfTrajectoryControl;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace NeutronsScattering.ViewModel
{
    //Viewmodel окна
    public class Lab1PracticeViewModel: ViewModelBase
    {
        private readonly IPracticeModel _model;
        private readonly INavigationService _navService;

        private OxyPlot.Series.LineSeries chartSeriesEnergy;
        private OxyPlot.Series.LineSeries chartSeriesU;
        private LinearAxis xAxis;
        private LinearAxis yAxisE;
        private LinearAxis yAxisU;

        #region Bindings
        
        public DataTable ExperimentsResults { get; set; }
        public PlotModel OxyModel { get; private set; }
        public TrajectoryViewerModel TrajectoryModel { get; private set; }

        private RelayCommand _updateModel;
        public ICommand UpdateModel
        {
            get
            {
                if (_updateModel == null)
                {
                    _updateModel = new RelayCommand(() => this.UploadChanges());
                }
                return _updateModel;
            }
        }

        private double _startEnergy;
        public double StartEnergy
        {
            get { return _startEnergy; }
            set
            {
                Set("StartEnergy", ref _startEnergy, value);
            }
        }

        private double _finalEnergy;
        public double FinalEnergy
        {
            get { return _finalEnergy; }
            set
            {
                Set("FinalEnergy", ref _finalEnergy, value);
            }
        }

        private RelayCommand<int> _changeGraph;
        public ICommand ChangeGraph
        {
            get
            {
                if (_changeGraph == null)
                {
                    _changeGraph = new RelayCommand<int>(param => ChangePlotSeries(param));
                }
                return _changeGraph;
            }
        }

        private RelayCommand _resetPlotZoom;
        public ICommand ResetPlotZoom
        {
            get
            {
                if (_resetPlotZoom == null)
                {
                    _resetPlotZoom = new RelayCommand(() => ResetOxyZoom(),() => ExperimentsResults.Rows.Count > 0);
                }
                return _resetPlotZoom;
            }
        }

        private IEnergyUnit _initEnergyUnit;
        public IEnergyUnit InitEnergyUnit
        {
            get { return _initEnergyUnit; }
            set { Set("InitEnergyUnit", ref _initEnergyUnit, value); }
        }

        private IEnergyUnit _finalEnergyUnit;
        public IEnergyUnit FinalEnergyUnit
        {
            get { return _finalEnergyUnit; }
            set { Set("FinalEnergyUnit", ref _finalEnergyUnit, value); }
        }

        private IChemElement _usingElement;
        public IChemElement UsingElement
        {
            get { return _usingElement; }
            set { Set("UsingElement", ref _usingElement, value); }
        }
        #endregion

        public Lab1PracticeViewModel(INavigationService navService)
        {
            _navService = navService;
            _model = new PracticeModel();
            _model.ModelUpdated += _model_ModelUpdated;

            CreateTableSchema();
            OxyModel = new PlotModel();
            
            xAxis = new LinearAxis();
            xAxis.Title = "N - номер столкновения";
            xAxis.Key = "Naxis";
            xAxis.Position = AxisPosition.Bottom;
            OxyModel.Axes.Add(xAxis);

            yAxisE = new LinearAxis();
            yAxisE.Title = "E - энергия после столкновения";
            yAxisE.Key = "Eaxis";
            yAxisE.Position = AxisPosition.Left;
            OxyModel.Axes.Add(yAxisE);

            yAxisU = new LinearAxis();
            yAxisU.Title = "u - потеря энергии";
            yAxisU.Key = "Uaxis";
            yAxisU.Position = AxisPosition.Left;
            yAxisU.IsAxisVisible = false;
            OxyModel.Axes.Add(yAxisU);

            chartSeriesEnergy = new OxyPlot.Series.LineSeries();
            chartSeriesEnergy.XAxisKey = "Naxis";
            chartSeriesEnergy.YAxisKey = "Eaxis";
            chartSeriesEnergy.TrackerFormatString = "N: {2:F0}\n\rE: {4:F0}";
            chartSeriesU = new OxyPlot.Series.LineSeries();
            chartSeriesU.XAxisKey = "Naxis";
            chartSeriesU.YAxisKey = "Uaxis";
            chartSeriesU.TrackerFormatString = "N: {2:F0}\n\rU: {4:F0}";
            chartSeriesU.IsVisible = false;
            OxyModel.Series.Add(chartSeriesEnergy);
            OxyModel.Series.Add(chartSeriesU);

            ViewModelsVariables.MainVM.BeforeNavigating += MainVM_BeforeNavigating;

            TrajectoryModel = new TrajectoryViewerModel();

            InitEnergyUnit = EnergyUnits.eV;
            FinalEnergyUnit = EnergyUnits.eV;
        }

        void MainVM_BeforeNavigating(object sender, BegoreNavigateArgs e)
        {
            if (e.TargetPageKey == "LabPart1")
            {
                OxyModel = new PlotModel();
                OxyModel.Axes.Add(xAxis);
                OxyModel.Axes.Add(yAxisE);
                OxyModel.Axes.Add(yAxisU);
                OxyModel.Series.Add(chartSeriesEnergy);
                OxyModel.Series.Add(chartSeriesU);
            }
            else if (e.CurrentPageKey == "LabPart1")
            {
                OxyModel.Series.Clear();
                OxyModel.Axes.Clear();
                OxyModel = null;
            }
        }

        private void ChangePlotSeries(int plotInd)
        {
            switch (plotInd)
            {
                case 0:
                    chartSeriesEnergy.IsVisible = true;
                    yAxisE.IsAxisVisible = true;
                    chartSeriesU.IsVisible = false;
                    yAxisU.IsAxisVisible = false;
                    break;
                default:
                    chartSeriesEnergy.IsVisible = false;
                    yAxisE.IsAxisVisible = false;
                    chartSeriesU.IsVisible = true;
                    yAxisU.IsAxisVisible = true;
                    break;
            }
            OxyModel.ResetAllAxes();
            OxyModel.InvalidatePlot(true);
        }

        private void ResetOxyZoom()
        {
            OxyModel.ResetAllAxes();
            OxyModel.InvalidatePlot(true);
        }

        void _model_ModelUpdated(object sender, ModelUpdateEventArgs e)
        {
            ExperimentsResults.Rows.Clear();
            chartSeriesEnergy.Points.Clear();
            chartSeriesU.Points.Clear();
            TrajectoryModel.Angles.Clear();
            var numScat = 0;

            var prnModel = ViewModelsVariables.MainVM.PrintModel;
            prnModel.UsingElement = UsingElement;
            prnModel.StartEnergy = StartEnergy;
            prnModel.StartUnits = InitEnergyUnit;
            prnModel.EndEnergy = FinalEnergy;
            prnModel.EndUnits = FinalEnergyUnit;

            e.Experiments.ForEach(x => {
                AppendDataRow(x);
                numScat++;
                chartSeriesEnergy.Points.Add(new DataPoint(numScat, x.EndEnergy));
                chartSeriesU.Points.Add(new DataPoint(numScat, x.U));
                TrajectoryModel.Angles.Add(x.Psi);
            });
            OxyModel.InvalidatePlot(true);
            TrajectoryModel.Redraw();

            byte[] plotEofN;
            byte[] plotUofN;
            ChangePlotSeries(0);
            using (MemoryStream ms = new MemoryStream()){
                OxyPlot.Wpf.PngExporter.Export(OxyModel, ms, 800, 400, OxyColor.FromRgb(255,255,255));
                plotEofN = ms.ToArray();
            }
            ChangePlotSeries(1);
            using (MemoryStream ms = new MemoryStream()){
                OxyPlot.Wpf.PngExporter.Export(OxyModel, ms, 800, 400, OxyColor.FromRgb(255,255,255));
                plotUofN = ms.ToArray();
            }
            ChangePlotSeries(0);

            prnModel.SetLabPract(plotEofN, plotUofN, e.Experiments);

            var newRow = ExperimentsResults.NewRow();
            newRow["Num"] = "Среднее";
            newRow["CosPsi"] = e.Experiments.Average(i => i.CosPsi);
            newRow["CosTheta"] = e.Experiments.Average(i => i.CosTheta);
            newRow["RelativeEnergyLoss"] = e.Experiments.Average(i => i.RelativeEnergyLoss);
            newRow["Xi"] = e.Experiments.Average(i => i.Xi);
            newRow["U"] = e.Experiments.Average(i => i.U);
            ExperimentsResults.Rows.Add(newRow);
        }

        public void UploadChanges()
        {
            var initEnergy = _startEnergy * InitEnergyUnit.Factor;
            var finalEnergy = _finalEnergy * FinalEnergyUnit.Factor;
            if (UsingElement == null)
            {
                MessageBox.Show("Выберите нуклид.", "Не выбран нуклид");
                return;
            }

            if (finalEnergy >= initEnergy || finalEnergy <= 0)
            {
                MessageBox.Show("Величина начальной энергии должна быть больше конечной. Конечная и начальная энергия должны быть положительными величинами.", "Некорректные начальные значения");
                return;
            }

            Messenger.Default.Send(new ElementInfo(this));
            _model.Update(UsingElement, initEnergy, finalEnergy);
        }

        private void CreateTableSchema()
        {
            ExperimentsResults = new DataTable();
            DataColumn column;

            column = new DataColumn();
            column.ColumnName = "Num";
            column.DataType = System.Type.GetType("System.String");
            column.Unique = true;
            column.ReadOnly = true;
            column.Caption = "N - номер столкновения";
            ExperimentsResults.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "CosPsi";
            column.DataType = System.Type.GetType("System.Double");
            column.ReadOnly = true;
            column.Caption = "cos ⁡θ";
            ExperimentsResults.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "CosTheta";
            column.DataType = System.Type.GetType("System.Double");
            column.ReadOnly = true;
            column.Caption = "cos⁡ ψ";
            ExperimentsResults.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "FinalEnergy";
            column.DataType = System.Type.GetType("System.Double");
            column.ReadOnly = true;
            column.Caption = "E";
            ExperimentsResults.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "RelativeEnergyLoss";
            column.DataType = System.Type.GetType("System.Double");
            column.ReadOnly = true;
            column.Caption = "u=ln⁡〖E_0/E〗";
            ExperimentsResults.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "Xi";
            column.DataType = System.Type.GetType("System.Double");
            column.ReadOnly = true;
            column.Caption = "ΔE/E";
            ExperimentsResults.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "U";
            column.DataType = System.Type.GetType("System.Double");
            column.ReadOnly = true;
            column.Caption = "ln⁡〖E_i/E_(i-1) 〗";
            ExperimentsResults.Columns.Add(column);
        }

        private void AppendDataRow(IPracticeCalcElement lRow)
        {
            var newRow = ExperimentsResults.NewRow();
            newRow["Num"] = ExperimentsResults.Rows.Count + 1;
            newRow["CosPsi"] = lRow.CosPsi;
            newRow["CosTheta"] = lRow.CosTheta;
            newRow["FinalEnergy"] = lRow.EndEnergy;
            newRow["RelativeEnergyLoss"] = lRow.RelativeEnergyLoss;
            newRow["Xi"] = lRow.Xi;
            newRow["U"] = lRow.U;
            ExperimentsResults.Rows.Add(newRow);
        }
    }
}
