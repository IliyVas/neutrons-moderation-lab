﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using NeutronsScattering.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;

namespace NeutronsScattering.ViewModel
{
    public class Lab1PrintViewModel : ViewModelBase
    {
        private readonly INavigationService _navService;

        public bool PrintFormGenerated { get; set; }

        private FlowDocument _printDoc;
        public FlowDocument PrintDoc
        {
            get { return _printDoc; }
            set { Set("PrintDoc", ref _printDoc, value); }
        }

        private RelayCommand _buildPrintForm;
        public ICommand BuildPrintForm
        {
            get
            {
                if (_buildPrintForm == null)
                {
                    _buildPrintForm = new RelayCommand(() => GenerateDoc());
                }
                return _buildPrintForm;
            }
        }

        private RelayCommand _printPDF;
        public ICommand PrintPDF
        {
            get
            {
                if (_printPDF == null)
                {
                    _printPDF = new RelayCommand(() => ExportPDF(), () => false);
                }
                return _printPDF;
            }
        }

        private RelayCommand _printXPS;
        public ICommand PrintXPS
        {
            get
            {
                if (_printXPS == null)
                {
                    _printXPS = new RelayCommand(() => ExportXPS(), () => PrintFormGenerated);
                }
                return _printXPS;
            }
        }

        public void GenerateDoc()
        {
            PrintDoc = System.Windows.Application.LoadComponent(new Uri("/Resources/Report.xaml", UriKind.RelativeOrAbsolute)) as FlowDocument;
            var prnModel = ViewModelsVariables.MainVM.PrintModel;

            if (ViewModelsVariables.MainVM.PrintModel.ReadyToPrint)
            {
                (PrintDoc.FindName("UsingElement1") as Run).Text = prnModel.UsingElement.Name;
                (PrintDoc.FindName("StartEnergy1") as Run).Text = prnModel.StartEnergy.ToString() + " " + prnModel.StartUnits.Name;
                (PrintDoc.FindName("FinalEnergy1") as Run).Text = prnModel.EndEnergy.ToString() + " " + prnModel.EndUnits.Name;

                var flwTable = (PrintDoc.FindName("ResultingTableLab1") as Table);
                List<IPracticeCalcElement> practResults;
                prnModel.GetPractLabTable(out practResults);

                Func<string, bool, TableCell> crTableVell = (cellStr, firstCell) =>
                {
                    var tblCell = new TableCell(new Paragraph(new Run(cellStr)));
                    tblCell.BorderBrush = System.Windows.Media.Brushes.Black;
                    tblCell.BorderThickness = new System.Windows.Thickness(firstCell ? 0 : 1, 1, 0, 0);
                    tblCell.Padding = new System.Windows.Thickness(5, 0, 5, 0);
                    return tblCell;
                };

                int numScat = 0;
                practResults.ForEach(x =>
                {
                    var row = new TableRow();

                    row.Cells.Add(crTableVell((++numScat).ToString(), true) );
                    row.Cells.Add(crTableVell(x.CosTheta.ToString("0.000"), false));
                    row.Cells.Add(crTableVell(x.CosPsi.ToString("0.000"), false));
                    row.Cells.Add(crTableVell(EnergyUnits.GetStringWithEnergyUnits(x.EndEnergy), false));
                    row.Cells.Add(crTableVell(x.RelativeEnergyLoss.ToString("0.000"), false));
                    row.Cells.Add(crTableVell(x.Xi.ToString("0.000"), false));
                    row.Cells.Add(crTableVell(x.U.ToString("0.000"), false));

                    flwTable.RowGroups[0].Rows.Add(row);
                });

                var avRow = new TableRow();
                avRow.Cells.Add(crTableVell("Среднее", true));
                avRow.Cells.Add(crTableVell(practResults.Average(i => i.CosTheta).ToString("0.000"), false));
                avRow.Cells.Add(crTableVell(practResults.Average(i => i.CosPsi).ToString("0.000"), false));
                avRow.Cells.Add(crTableVell("", false));
                avRow.Cells.Add(crTableVell(practResults.Average(i => i.RelativeEnergyLoss).ToString("0.000"), false));
                avRow.Cells.Add(crTableVell(practResults.Average(i => i.Xi).ToString("0.000"), false));
                avRow.Cells.Add(crTableVell(practResults.Average(i => i.U).ToString("0.000"), false));
                flwTable.RowGroups[0].Rows.Add(avRow);

                byte[] buf;
                prnModel.GetImgEofN(out buf);

                var bitmap1 = new BitmapImage();
                using (MemoryStream ms = new MemoryStream(buf))
                {
                    bitmap1.BeginInit();
                    bitmap1.CacheOption = BitmapCacheOption.OnLoad;
                    bitmap1.StreamSource = ms;
                    bitmap1.EndInit();
                }

                (PrintDoc.FindName("plotEfromN") as Image).Source = bitmap1;

                prnModel.GetImgUofN(out buf);
                var bitmap2 = new BitmapImage();
                using (MemoryStream ms = new MemoryStream(buf))
                {
                    bitmap2.BeginInit();
                    bitmap2.CacheOption = BitmapCacheOption.OnLoad;
                    bitmap2.StreamSource = ms;
                    bitmap2.EndInit();
                }

                (PrintDoc.FindName("plotUfromN") as Image).Source = bitmap2;

                PrintFormGenerated = true;
            }
        }

        public void ExportPDF()
        {
            Microsoft.Win32.SaveFileDialog svDlg = new Microsoft.Win32.SaveFileDialog();
            svDlg.FileName = "Результаты моделирования лабораторной работы Замедление нейтронов";
            svDlg.DefaultExt = ".pdf";
            svDlg.Filter = "Portable Document Format (.pdf)|*.pdf";

            Nullable<bool> isSvd = svDlg.ShowDialog();

            if (isSvd == true)
            {
            }
        }

        public void ExportXPS()
        {
            Microsoft.Win32.SaveFileDialog svDlg = new Microsoft.Win32.SaveFileDialog();
            svDlg.FileName = "Результаты моделирования лабораторной работы Замедление нейтронов";
            svDlg.DefaultExt = ".xps";
            svDlg.Filter = "XML Paper Specification (.xps)|*.xps";

            Nullable<bool> isSvd = svDlg.ShowDialog();
            if (isSvd == true)
            {
                var xpsFileName = svDlg.FileName;

                using (var xpsFileWriter = new FileStream(xpsFileName, FileMode.Create))
                {
                    using (var package = Package.Open(xpsFileWriter, FileMode.Create, FileAccess.ReadWrite))
                    {
                        using (var xpsDoc = new XpsDocument(package, CompressionOption.Maximum))
                        {
                            var rsm = new XpsSerializationManager(new XpsPackagingPolicy(xpsDoc), false);
                            var paginator = ((IDocumentPaginatorSource)PrintDoc).DocumentPaginator;
                            rsm.SaveAsXaml(paginator);
                            rsm.Commit();
                        }
                    }
                }

                _navService.NavigateTo("Intro");
                System.Windows.MessageBox.Show("Файл успешно сохранен"); 
            }
        }

        public Lab1PrintViewModel(INavigationService navService)
        {
            _navService = navService;

            PrintDoc = new FlowDocument();
            PrintFormGenerated = false;
        }
    }
}
