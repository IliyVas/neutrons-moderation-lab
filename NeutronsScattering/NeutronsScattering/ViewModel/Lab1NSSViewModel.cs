﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using NeutronsScattering.Model;
using NeutronsScattering.ViewModel.Navigation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace NeutronsScattering.ViewModel
{
    public class Lab1NSSViewModel : ViewModelBase
    {
        private readonly INavigationService _navService;
        private ISpectrumModel _model;

        #region Привязки
        public DataTable ScatteringSpectrum { get; set; }

        private IChemElement _usingElement;
        public IChemElement UsingElement
        {
            get { return _usingElement; }
            set { Set("UsingElement", ref _usingElement, value); }
        }

        private double _startEnergy;
        public double StartEnergy
        {
            get { return _startEnergy; }
            set
            {
                Set("StartEnergy", ref _startEnergy, value);
            }
        }

        private double _finalEnergy;
        public double FinalEnergy
        {
            get { return _finalEnergy; }
            set
            {
                Set("FinalEnergy", ref _finalEnergy, value);
            }
        }

        private IEnergyUnit _initEnergyUnit;
        public IEnergyUnit InitEnergyUnit
        {
            get { return _initEnergyUnit; }
            set { Set("InitEnergyUnit", ref _initEnergyUnit, value); }
        }

        private IEnergyUnit _finalEnergyUnit;
        public IEnergyUnit FinalEnergyUnit
        {
            get { return _finalEnergyUnit; }
            set { Set("FinalEnergyUnit", ref _finalEnergyUnit, value); }
        }

        private int _numNeutronFates;
        public int NumNeutronFates
        {
            get { return _numNeutronFates; }
            set { Set("NumNeutronFates", ref _numNeutronFates, value); }
        }

        private RelayCommand _buildSpectrum;
        public ICommand BuildSpectrum
        {
            get
            {
                if (_buildSpectrum == null)
                {
                    _buildSpectrum = new RelayCommand(() => UpdateModel());
                }
                return _buildSpectrum;
            }
        }
        #endregion

        public Lab1NSSViewModel(INavigationService navService)
        {
            _navService = navService;
            Messenger.Default.Register<ElementInfo>(this, ElementChanged);
            _model = new SpectrumModel();
            _model.ModelUpdated += _model_ModelUpdated;
            CreateTableSchema();
        }

        private void ElementChanged(ElementInfo e)
        {
            UsingElement = e.UsingElement;
            StartEnergy = e.StartEnergy;
            InitEnergyUnit = e.InitEnergyUnit;
            FinalEnergy = e.FinalEnergy;
            FinalEnergyUnit = e.FinalEnergyUnit;
        }

        private void _model_ModelUpdated(object sender, SpectrumModelUpdateEventArgs e)
        {
            ScatteringSpectrum.Rows.Clear();
            e.spectrum.ForEach(x => AppendDataRow(x));
        }

        public void UpdateModel()
        {
            if (UsingElement == null)
            {
                MessageBox.Show("Выполните первую часть лабораторной.");
                return;
            }

            if (NumNeutronFates <= 0)
            {
                MessageBox.Show("Укажите число судеб.");
                return;
            }

            _model.UsingElement = UsingElement;
            _model.NeutronsFates = NumNeutronFates;
            _model.InitialEnergy = StartEnergy * InitEnergyUnit.Factor;
            _model.FinalEnergy = FinalEnergy * FinalEnergyUnit.Factor;
            _model.Generate();
        }

        private void AppendDataRow(ISpectrumCalcElement lRow)
        {
            var newRow = ScatteringSpectrum.NewRow();
            newRow["NeutronFate"] = lRow.NeutronFate;
            newRow["AverCosTheta"] = lRow.AverCosTheta;
            newRow["AverCosPsi"] = lRow.AverCosPsi;
            newRow["AverXi"] = lRow.AverXi;
            newRow["AverU"] = lRow.AverU;
            newRow["AverNumScattering"] = lRow.AverNumScattering;
            ScatteringSpectrum.Rows.Add(newRow);
            
        }

        private void CreateTableSchema()
        {
            ScatteringSpectrum = new DataTable();
            DataColumn column;

            column = new DataColumn();
            column.ColumnName = "NeutronFate";
            column.DataType = System.Type.GetType("System.Int32");
            column.Unique = true;
            column.ReadOnly = true;
            column.Caption = "N – число судеб нейтронов";
            ScatteringSpectrum.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "AverCosTheta";
            column.DataType = System.Type.GetType("System.Double");
            column.Unique = false;
            column.ReadOnly = true;
            column.Caption = "Среднее cos⁡ θ";
            ScatteringSpectrum.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "AverCosPsi";
            column.DataType = System.Type.GetType("System.Double");
            column.Unique = false;
            column.ReadOnly = true;
            column.Caption = "Среднее cos⁡ ψ";
            ScatteringSpectrum.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "AverXi";
            column.DataType = System.Type.GetType("System.Double");
            column.Unique = false;
            column.ReadOnly = true;
            column.Caption = "Среднее ΔE/E";
            ScatteringSpectrum.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "AverU";
            column.DataType = System.Type.GetType("System.Double");
            column.Unique = false;
            column.ReadOnly = true;
            column.Caption = "Среднее ln⁡〖E_i/E_(i-1) 〗";
            ScatteringSpectrum.Columns.Add(column);

            
            column = new DataColumn();
            column.ColumnName = "AverNumScattering";
            column.DataType = System.Type.GetType("System.Double");
            column.Unique = false;
            column.ReadOnly = true;
            column.Caption = "N - ср. число столкновений";
            ScatteringSpectrum.Columns.Add(column);
        }
    }
}
