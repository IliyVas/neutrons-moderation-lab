﻿using NeutronsScattering.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NeutronsScattering.ViewModel
{
    //Класс для расчетов положений нейтрона и ядра после анимаци
    public class AnimationValueCalculator
    {
        public double SizeElem { get; set; }
        
        private double SizeFieldX { get; set; }
        private double SizeFieldY { get; set; }

        // Границы выхода
        double edgeX;
        double edgeY;

        public double CoordinatsX { get; private set; }
        public double CoordinatsY { get; private set; }
        public double TrajectoryX { get; private set; }
        public double TrajectoryY { get; private set; }

        //Расcчитывает значения координат конечной точки движения для движущегося
        //объекта и его траектории (пунктирной линии)
        public void CalculateAnimationValues(double theta, double energyChangeVal)
        {
            double xTrOut = 0;
            double yTrOut = 0;

            var dist = 220 * 2 * Math.Sqrt(energyChangeVal);

            if (Math.Abs(theta - Math.PI / 2) < 0.001)
            {
                if (theta < Math.PI)
                {
                    xTrOut = 0;
                    yTrOut = edgeY;
                }
                else
                {
                    xTrOut = 0;
                    yTrOut = -edgeY;
                }
            }
            else
            {
                xTrOut = 500 * Math.Cos(theta);
                yTrOut = 500 * Math.Sin(theta);

                if (Math.Abs(xTrOut) > edgeX)
                {
                    xTrOut = Math.Sign(xTrOut) * edgeX;
                    yTrOut = Math.Tan(theta) * xTrOut;
                }

                if (Math.Abs(yTrOut) > edgeY)
                {
                    yTrOut = Math.Sign(yTrOut) * edgeY;
                    xTrOut = yTrOut / Math.Tan(theta);
                }
            }

            CoordinatsX = SizeFieldX + dist * Math.Cos(theta) - SizeElem;
            CoordinatsY = SizeFieldY - dist * Math.Sin(theta) - SizeElem;

            TrajectoryX = SizeFieldX + xTrOut;
            TrajectoryY = SizeFieldY - yTrOut;
        }

        public AnimationValueCalculator()
        {
            SizeElem = 5;
            ChangeSizeField(200, 150);
        }

        //Пересчет границ при масштабировании
        public void ChangeSizeField(double xSize, double ySize)
        {
            SizeFieldX = xSize;
            SizeFieldY = ySize;
            edgeX = SizeFieldX + SizeElem;
            edgeY = SizeFieldY + SizeElem;
        }
    }
}
