﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using Ionic.Zip;
using NeutronsScattering.Model;
using NeutronsScattering.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;

namespace NeutronsScattering.ViewModel
{
    public interface IMainWindowViewModel
    {
        event EventHandler<UsingElementUpdatedArgs> UsingElementUpdated;
        double ScaleValue { get; set; }
        IFrameNavigationService NavService { get; }
        PrintDataModel PrintModel { get; }
        bool BeforeNavigate(string currentPageKey, string targetPageKey);
        event EventHandler<BegoreNavigateArgs> BeforeNavigating;
    }

    public class UsingElementUpdatedArgs : EventArgs
    {
        public IChemElement NewElement { get; private set; }
        public UsingElementUpdatedArgs(IChemElement newEl)
        {
            NewElement = newEl;
        }
    }

    public class BegoreNavigateArgs : EventArgs
    {
        /// <summary>
        /// Ключ текущей страницы
        /// </summary>
        public string CurrentPageKey { get; private set; }
        /// <summary>
        /// Ключ страницы, на которую осуществляется переход
        /// </summary>
        public string TargetPageKey { get; private set; }
        /// <summary>
        /// Если установить false, то переход осуществлен не будет
        /// </summary>
        public bool Abort { get; set; }
        public BegoreNavigateArgs(string currentPageKey, string targetPageKey)
        {
            CurrentPageKey = currentPageKey;
            TargetPageKey = targetPageKey;
            Abort = false;
        }
    }

    public class MainWindowViewModel : PropertyChangedBase, IMainWindowViewModel
    {
        #region Навигация
        private readonly string[] pagesRoute = { "Intro", "LabPart1", "LabPrint" };
        public IFrameNavigationService NavService { get; private set; }
        
        private RelayCommand _nextPageCommand;
        public ICommand NextPageCommand
        {
            get
            {
                if (_nextPageCommand == null)
                {
                    _nextPageCommand = new RelayCommand(() => toNextPage(), mayNextPage);
                }
                return _nextPageCommand;
            }
        }
        private RelayCommand _prevPageCommand;
        public ICommand PrevPageCommand
        {
            get
            {
                if (_prevPageCommand == null)
                {
                    _prevPageCommand = new RelayCommand(() => toPrevPage(), mayPrevPage);
                }
                return _prevPageCommand;
            }
        }
        private bool mayNextPage()
        {
            return Array.IndexOf<string>(pagesRoute, NavService.CurrentPageKey) < pagesRoute.Length - 1;
        }
        private void toNextPage()
        {
            NavService.NavigateTo(pagesRoute[Array.IndexOf<string>(pagesRoute, NavService.CurrentPageKey) + 1]);
        }
        private bool mayPrevPage()
        {
            return Array.IndexOf<string>(pagesRoute, NavService.CurrentPageKey) > 0;
        }
        private void toPrevPage()
        {
            NavService.NavigateTo(pagesRoute[Array.IndexOf<string>(pagesRoute, NavService.CurrentPageKey) - 1]);
        }
        public bool BeforeNavigate(string currentPageKey, string targetPageKey)
        {
            ElementSelectVisible = targetPageKey == "Intro" ? Visibility.Visible : Visibility.Collapsed;
            
            var param = new BegoreNavigateArgs(currentPageKey, targetPageKey);
            BeforeNavigating(this, param);
            return !param.Abort;
        }

        public event EventHandler<BegoreNavigateArgs> BeforeNavigating = delegate { };
        #endregion

        public event EventHandler<UsingElementUpdatedArgs> UsingElementUpdated = delegate { };

        public PrintDataModel PrintModel { get; set; }

        private double _scaleValue;
        public double ScaleValue
        {
            get { return _scaleValue; }
            set { SetField(ref _scaleValue, value, "ScaleValue"); }
        }
        public List<IChemElement> ListOfElements {
            get { return Chemestry.ElementsList; }
        }

        private Visibility _elementSelectVisible;
        public Visibility ElementSelectVisible
        {
            get { return _elementSelectVisible; }
            set { SetField(ref _elementSelectVisible, value, "ElementSelectVisible"); }
        }

        private RelayCommand _openLabBook;
        public ICommand OpenLabBook
        {
            get
            {
                if (_openLabBook == null)
                {
                    _openLabBook = new RelayCommand(() => OpenHelp());
                }
                return _openLabBook;
            }
        }

        public MainWindowViewModel(IFrameNavigationService navService)
        {
            NavService = navService;
            ElementSelectVisible = Visibility.Visible;
            PrintModel = new PrintDataModel();            
        }

        public void OpenHelp()
        {
            var appDataPath = Path.GetTempPath();

            using (MemoryStream _zipFileStream = new MemoryStream(Properties.Resources.NeutronScatteringHelp))
            {
                // use Ionic.Zip to open the zip file
                using (ZipFile zipFile = ZipFile.Read(_zipFileStream))
                {
                    try
                    {
                        zipFile.ExtractAll(appDataPath, ExtractExistingFileAction.DoNotOverwrite);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ошибка распаковки файлов справки!");
                        return;
                    }
                }

                System.Diagnostics.Process.Start(appDataPath + @"\NeutronsScatteringHelp\index.html");
            }
        }
    }
}
