﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using NeutronsScattering.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace NeutronsScattering.ViewModel
{
    //Viewmodel страницы, показывающейся при запуске
    public class IntroductionViewModel : ViewModelBase
    {
        private ICalculationModel _model;
        private AnimationValueCalculator _animationCalc;
        private Random elemGen;
        private readonly INavigationService _navService;

        #region Physics

        private IChemElement _usingElement;
        public IChemElement UsingElement
        {
            get { return _usingElement; }
            set { Set("UsingElement", ref _usingElement, value); }
        }

        private double _cosPsi;
        public double CosPsi {
            get { return _cosPsi; }
            set { Set("CosPsi", ref _cosPsi, value); }
        }

        private double _angleDif;
        public double AngleDif
        {
            get { return _angleDif; }
            set { Set("AngleDif", ref _angleDif, value); }
        }

        private double _valueE1;
        public double ValueE1
        {
            get { return _valueE1; }
            set { Set("ValueE1", ref _valueE1, value); }
        }

        private double _valueXi;
        public double ValueXi
        {
            get { return _valueXi; }
            set { Set("ValueXi", ref _valueXi, value);}
        }

        private double _energyBegin;
        public double EnergyBegin
        {
            get { return _energyBegin; }
            set { Set("EnergyBegin", ref _energyBegin, value); }
        }

        private double _energyEnd;
        public double EnergyEnd
        {
            get { return _energyEnd; }
            set { Set("EnergyEnd", ref _energyEnd, value); }
        }
        #endregion

        #region Animation

        #region Animation Bindings
        private bool _animationStopped;
        public string AnimationState
        {
            get
            {
                return _animationStopped ? "OFF" : "ON";
            }
            set
            {
                Set("AnimationState", ref _animationStopped, value.Equals("ON", StringComparison.OrdinalIgnoreCase) ? false : true);
            }
        }

        private RelayCommand _animationFinish;
        public ICommand AnimationFinish
        {
            get
            {
                if (_animationFinish == null)
                {
                    _animationFinish = new RelayCommand(() => AnimationEnd());
                }
                return _animationFinish;
            }
        }

        private double _neuronOutTrajecX;
        public double NeuronOutTrajecX
        {
            get { return _neuronOutTrajecX; }
            set { Set("NeuronOutTrajecX", ref _neuronOutTrajecX, value); }
        }
        private double _neuronOutTrajecY;
        public double NeuronOutTrajecY
        {
            get { return _neuronOutTrajecY; }
            set { Set("NeuronOutTrajecY", ref _neuronOutTrajecY, value); }
        }
        private double _neuronOutCoordX;
        public double NeuronOutCoordX
        {
            get { return _neuronOutCoordX; }
            set { Set("NeuronOutCoordX", ref _neuronOutCoordX, value); }
        }
        private double _neuronOutCoordY;
        public double NeuronOutCoordY
        {
            get { return _neuronOutCoordY; }
            set { Set("NeuronOutCoordY", ref _neuronOutCoordY, value); }
        }
        private KeyTime _neuronInTime;
        public KeyTime NeuronInTime
        {
            get { return _neuronInTime; }
            set { Set("NeuronInTime", ref _neuronInTime, value); }
        }
        private KeyTime _neuronOutTime;
        public KeyTime NeuronOutTime
        {
            get { return _neuronOutTime; }
            set { Set("NeuronOutTime", ref _neuronOutTime, value); }
        }
        private Point _angleVisual;
        public Point AngleVisual
        {
            get { return _angleVisual; }
            set { Set("AngleVisual", ref _angleVisual, value); }
        }
        private double _angleCaptionX;
        public double AngleCaptionX
        {
            get { return _angleCaptionX; }
            set { Set("AngleCaptionX", ref _angleCaptionX, value); }
        }
        private double _angleCaptionY;
        public double AngleCaptionY
        {
            get { return _angleCaptionY; }
            set { Set("AngleCaptionY", ref _angleCaptionY, value); }
        }
        private double _nuclidOutTrajecX;
        public double NuclidOutTrajecX
        {
            get { return _nuclidOutTrajecX; }
            set { Set("NuclidOutTrajecX", ref _nuclidOutTrajecX, value); }
        }
        private double _nuclidOutTrajecY;
        public double NuclidOutTrajecY
        {
            get { return _nuclidOutTrajecY; }
            set { Set("NuclidOutTrajecY", ref _nuclidOutTrajecY, value); }
        }
        private double _nuclidOutCoordX;
        public double NuclidOutCoordX
        {
            get { return _nuclidOutCoordX; }
            set { Set("NuclidOutCoordX", ref _nuclidOutCoordX, value); }
        }
        private double _nuclidOutCoordY;
        public double NuclidOutCoordY
        {
            get { return _nuclidOutCoordY; }
            set { Set("NuclidOutCoordY", ref _nuclidOutCoordY, value); }
        }
        private bool _largeAngle;
        public bool LargeAngle
        {
            get { return _largeAngle; }
            set { Set("LargeAngle", ref _largeAngle, value); }
        }
        private double _nuclidSize;
        public double NuclidSize
        {
            get { return _nuclidSize; }
            set { Set("NuclidSize", ref _nuclidSize, value); }
        }
        private double _nuclidStartPositionX;
        public double NuclidStartPositionX
        {
            get { return _nuclidStartPositionX; }
            set { Set("NuclidStartPositionX", ref _nuclidStartPositionX, value); }
        }
        private double _nuclidStartPositionY;
        public double NuclidStartPositionY
        {
            get { return _nuclidStartPositionY; }
            set { Set("NuclidStartPositionY", ref _nuclidStartPositionY, value); }
        }
        #endregion

        public void AnimationStart()
        {
            AnimationState = "ON";
        }

        public void AnimationEnd()
        {
            AnimationState = "OFF";
            CalcScattering();
        }

        private void CalculateAnimationValues()
        {
            _animationCalc.SizeElem = 5;
            _animationCalc.CalculateAnimationValues(_model.Theta, _model.EndEnergy / _model.StartEnergy);

            NeuronOutCoordX = _animationCalc.CoordinatsX;
            NeuronOutCoordY = _animationCalc.CoordinatsY;
            NeuronOutTrajecX = _animationCalc.TrajectoryX;
            NeuronOutTrajecY = _animationCalc.TrajectoryY;

            _animationCalc.SizeElem = NuclidSize / 2;

            double nuclidAngle;
            if (Math.Abs(_model.Theta - Math.PI) < Math.PI / 2)
                nuclidAngle = _model.Theta - Math.PI;
            else
                nuclidAngle = 2 * Math.PI - _model.Theta;

            _animationCalc.CalculateAnimationValues(nuclidAngle, (_model.StartEnergy - _model.EndEnergy) / _model.StartEnergy / UsingElement.MassNumber);

            NuclidOutCoordX = _animationCalc.CoordinatsX;
            NuclidOutCoordY = _animationCalc.CoordinatsY;
            NuclidOutTrajecX = _animationCalc.TrajectoryX;
            NuclidOutTrajecY = _animationCalc.TrajectoryY;

            // Считаем точку дуги, показывающей угол
            var xAnglePoint = 50 * _model.CosTheta;
            var yAnglePoint = 50 * Math.Sin(_model.Theta);
            AngleVisual = new Point(xAnglePoint + 200, 150 - yAnglePoint);
            LargeAngle = _model.Theta > Math.PI;

            // Считаем координаты надписи угла
            var xAngleCaption = 60d;
            var yAngleCaption = 10d;
            if (AngleDif > 15)
            {
                xAngleCaption = 60 * Math.Cos(_model.Theta / 2);
                yAngleCaption = 60 *Math.Sin(_model.Theta / 2);
            }
            AngleCaptionX = 200 + xAngleCaption + (_model.Theta > Math.PI ? -70 : 0);
            AngleCaptionY = 150 - yAngleCaption - (_model.Theta > Math.PI ? 0 : 10);

            NeuronInTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1));
            NeuronOutTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(3));
        }

        private void ChangeSizeNuclid(double newSize)
        {
            NuclidSize = newSize;
            NuclidStartPositionX = 200 - newSize / 2;
            NuclidStartPositionY = 150 - newSize / 2;
        }
        #endregion

        public void Update()
        {
            CosPsi = _model.CosPsi;
            AngleDif = 180 * _model.Theta / Math.PI;
            ValueE1 = _model.EndEnergy;
            ValueXi = _model.Xi;
            EnergyBegin = _model.StartEnergy;
            EnergyEnd = _model.EndEnergy;
        }

        public IntroductionViewModel(INavigationService navService)
        {
            _navService = navService;
            _model = new CalculationModel();
            _model.ModelUpdated += _model_ModelUpdated;
            UsingElement = Chemestry.H1;
            ChangeSizeNuclid(UsingElement.RelativeSize);
            _model.ChangeInitialValues(UsingElement, 10000, 1000);
            elemGen = new Random();
            _animationCalc = new AnimationValueCalculator();
        }

        private void _model_ModelUpdated(object sender, EventArgs e)
        {
            Update();
            CalculateAnimationValues();
            AnimationStart();
        }

        public void CalcScattering()
        {
            if (_model.EndEnergy < _model.FinalEnergy)
            {
                UsingElement = Chemestry.ElementsList[elemGen.Next(Chemestry.ElementsList.Count - 1)];
                ChangeSizeNuclid(UsingElement.RelativeSize);
                _model.ChangeInitialValues(UsingElement, 10000, 1000);
            }
            _model.NextValues();
        }
    }
}
