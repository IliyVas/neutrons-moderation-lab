﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace NeutronsScattering.Model
{
    public interface IEnergyUnit
    {
        string Name { get; }
        double Factor { get; }
    }

    public struct EnergyUnit : IEnergyUnit
    {
        public string Name { get; private set; }
        public double Factor { get; private set; }
        public EnergyUnit(string name, double factor) : this()
        {
            Name = name;
            Factor = factor;
        }
    }

    public static class EnergyUnits
    {
        public static readonly EnergyUnit eV = new EnergyUnit(name: "эВ", factor: 1d);
        public static readonly EnergyUnit keV = new EnergyUnit(name: "кэВ", factor: 1E3);
        public static readonly EnergyUnit MeV = new EnergyUnit(name: "МэВ", factor: 1E6);

        public static ReadOnlyCollection<IEnergyUnit> UnitsList = new ReadOnlyCollection<IEnergyUnit>(
            new List<IEnergyUnit>() { eV, MeV }
        );

        public static string GetStringWithEnergyUnits(double enrgy)
        {
            if (enrgy > MeV.Factor)
            {
                return (enrgy / MeV.Factor).ToString("0.0") + " " + MeV.Name;
            }
            else if (enrgy > keV.Factor)
            {
                return (enrgy / keV.Factor).ToString("0.0") + " " + keV.Name;
            }
            else
            {
                return (enrgy).ToString("0.0") + " " + eV.Name;
            }
        }
    }
}
