﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NeutronsScattering.Model
{
    interface ISpectrumModel
    {
        void Generate();
        event EventHandler<SpectrumModelUpdateEventArgs> ModelUpdated;
        int NeutronsFates { get; set; }
        IChemElement UsingElement { get; set; }
        /// <summary>Начальная энергия</summary>
        double InitialEnergy { get; set; }
        /// <summary>Конечная энергия</summary>
        double FinalEnergy { get; set; }
    }

    public class SpectrumModelUpdateEventArgs : EventArgs
    {
        public List<ISpectrumCalcElement> spectrum { get; private set; }
        public SpectrumModelUpdateEventArgs(List<ISpectrumCalcElement>  _spectrum)
        {
            spectrum = _spectrum;
        }
    }

    public interface ISpectrumCalcElement
    {
        int NeutronFate { get; }
        double AverCosTheta { get; }
        double AverCosPsi { get; }
        double AverXi { get; }
        double AverU { get; }
        double AverNumScattering { get; }
    }

    public class SpectrumCalcElement : ISpectrumCalcElement
    {
        public int NeutronFate { get; set; }
        public double AverCosTheta { get; set; }
        public double AverCosPsi { get; set; }
        public double AverXi { get; set; }
        public double AverU { get; set; }
        public double AverNumScattering { get; set; }
    }

    public class SpectrumModel : ISpectrumModel
    {
        public event EventHandler<SpectrumModelUpdateEventArgs> ModelUpdated = delegate { };
        public int NeutronsFates { get; set; }
        public IChemElement UsingElement { get; set; }
        /// <summary>Начальная энергия</summary>
        public double InitialEnergy { get; set; }
        /// <summary>Конечная энергия</summary>
        public double FinalEnergy { get; set; }
        private static RNGCryptoServiceProvider _globalRnd = new RNGCryptoServiceProvider();

        public void Generate()
        {
            ConcurrentBag<ISpectrumCalcElement> listOfResults = new ConcurrentBag<ISpectrumCalcElement>();
            ThreadLocal<ICalculationModel> calcModel = new ThreadLocal<ICalculationModel>(() => new CalculationModel(UsingElement, InitialEnergy, FinalEnergy));
            ThreadLocal<List<IPracticeCalcElement>> calcList = new ThreadLocal<List<IPracticeCalcElement>>(() => new List<IPracticeCalcElement>());
            Parallel.For(0, NeutronsFates, (i) =>
            {
                calcList.Value.Clear();
                var dataUint = new byte[sizeof(uint)];
                calcModel.Value.RestartCalc();

                Func<double> getRndDouble = () =>
                {
                    _globalRnd.GetBytes(dataUint);
                    var randUind = BitConverter.ToUInt32(dataUint, 0);
                    return randUind / (uint.MaxValue + 1.0);
                };

                while (calcModel.Value.NextValues(getRndDouble()))
                {
                    calcList.Value.Add(new PracticeCalcElement(calcModel.Value));
                }

                var result = new SpectrumCalcElement();
                result.NeutronFate = i + 1;
                result.AverCosPsi = calcList.Value.Average((el) => el.CosPsi);
                result.AverCosTheta = calcList.Value.Average((el) => el.CosTheta);
                result.AverNumScattering = calcList.Value.Count;
                result.AverU = calcList.Value.Average((el) => el.U);
                result.AverXi = calcList.Value.Average((el) => el.Xi);
                listOfResults.Add(result);
            });

            var retArg = new SpectrumModelUpdateEventArgs(listOfResults.OrderBy((el) => el.NeutronFate).ToList());
            ModelUpdated(this, retArg);
        }
    }
}
