﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NeutronsScattering.Model
{
    public interface IRestoreCalculation
    {
        void SaveResults();
        void SaveResults(string name);
        void RestoreResults(double initial, double final);
        void RestoreResults(string name);
    }
    public class ModelUpdateEventArgs : EventArgs 
    {
        public List<IPracticeCalcElement> Experiments { get; private set; }
        public ModelUpdateEventArgs(List<IPracticeCalcElement> experiments)
        {
            Experiments = experiments;
        }
    }
    public interface IPracticeModel
    {
        event EventHandler<ModelUpdateEventArgs> ModelUpdated;
        event EventHandler NuclidChanged;
        bool IsNewValues(IChemElement element, double initial, double final);
        void Update(IChemElement element, double initial, double final);

        [Obsolete("Данный метод больше не поддерживается. Используйте Update(IChemElement, double, double).")]
        void Update();

        [Obsolete("Данное свойство вскоре будет удаленно.")]
        IChemElement UsingElement { get; set; }

        [Obsolete("Данный метод больше не поддерживается. Используйте ModelUpdated handler)")]
        List<IPracticeCalcElement> GetData();
    }

    public interface IPracticeCalcElement
    {
        double CosPsi { get; }
        double CosTheta { get; }
        double EndEnergy { get; }
        double RelativeEnergyLoss { get; }
        double U { get; }
        IChemElement UsingElement { get; set; }
        double Xi { get; }
        double Theta { get; }
        double Psi { get; }
    }

    public class PracticeCalcElement : IPracticeCalcElement
    {
        public IChemElement UsingElement { get; set; }
        public double CosTheta { get; private set; }
        public double CosPsi { get; private set; }
        public double EndEnergy { get; private set; }
        public double RelativeEnergyLoss { get; private set; }
        public double Xi { get; private set; }
        public double U { get; private set; }
        public double Theta { get; private set; }
        public double Psi { get; private set; }

        public PracticeCalcElement(ICalculationModel calc)
        {
            UsingElement = calc.UsingElement;
            CosTheta = calc.CosTheta;
            CosPsi = calc.CosPsi;
            EndEnergy = calc.EndEnergy;
            RelativeEnergyLoss = calc.RelativeEnergyLoss;
            Xi = calc.Xi;
            U = calc.U;
            Theta = calc.Theta;
            Psi = calc.Psi;
        }

        public PracticeCalcElement(IPracticeCalcElement el)
        {
            UsingElement = el.UsingElement;
            CosTheta = el.CosTheta;
            CosPsi = el.CosPsi;
            EndEnergy = el.EndEnergy;
            RelativeEnergyLoss = el.RelativeEnergyLoss;
            Xi = el.Xi;
            U = el.U;
            Theta = el.Theta;
            Psi = el.Psi;
        }
    }

    public class PracticeModel : IPracticeModel
    {
        public IChemElement UsingElement { get; set; }
        private List<IPracticeCalcElement> _experiments;
        private ICalculationModel _calculator;

        public event EventHandler<ModelUpdateEventArgs> ModelUpdated = delegate { };
        public event EventHandler NuclidChanged = delegate { };

        public PracticeModel()
        {
            _experiments = new List<IPracticeCalcElement>();
            _calculator = new CalculationModel();
        }
        
        public List<IPracticeCalcElement> GetData()
        {
            throw new NotSupportedException("Данный метод больше не поддерживается");
        }

        public bool IsNewValues(IChemElement element, double initial, double final)
        {
            return element != _calculator.UsingElement
                   || !(Math.Abs(_calculator.InitialEnergy  - initial) < 0.0000001)
                   || !(Math.Abs(_calculator.FinalEnergy    - initial) < 0.0000001);
        }

        public void Update()
        {
            throw new NotSupportedException("Данный метод больше не поддерживается");
        }

        public void Update(IChemElement element, double initial, double final)
        {
            if (element == null || !IsNewValues(element, initial, final)) 
                return; 

            _calculator.ChangeInitialValues(element, initial, final);
            _experiments.Clear();

            while (_calculator.EndEnergy > _calculator.FinalEnergy)
            {
                _calculator.NextValues();
                _experiments.Add(new PracticeCalcElement(_calculator));
            }

            var eventArgs = new ModelUpdateEventArgs(_experiments);
            ModelUpdated(this, eventArgs);
        }
    }
}
