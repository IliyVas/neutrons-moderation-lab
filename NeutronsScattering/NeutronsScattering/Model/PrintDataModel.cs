﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NeutronsScattering.Model
{
    public class PrintDataModel
    {
        private bool labPractCompl;
        private byte[] imgPlotEofN;
        private byte[] imgPlotUofN;
        private List<IPracticeCalcElement> tablNeuronScat;

        public IChemElement UsingElement { get; set; }
        public double StartEnergy { get; set; }
        public IEnergyUnit StartUnits { get; set; }
        public IEnergyUnit EndUnits { get; set; }
        public double EndEnergy { get; set; }
        public bool ReadyToPrint
        {
            get { return labPractCompl; }
        }

        public bool GetImgEofN(out byte[] stream)
        {
            stream = null;
            if (labPractCompl)
            {
                stream = imgPlotEofN;
            }
            return labPractCompl;
        }

        public bool GetImgUofN(out byte[] stream)
        {
            stream = null;
            if (labPractCompl)
            {
                stream = imgPlotUofN;
            }
            return labPractCompl;
        }

        public bool GetPractLabTable(out List<IPracticeCalcElement> tabl)
        {
            tabl = null;
            if (labPractCompl)
            {
                tabl = tablNeuronScat;
            }
            return labPractCompl;
        }

        public void SetLabPract(byte[] imgEofN, byte[] imgUofN, List<IPracticeCalcElement> tabl)
        {
            imgPlotEofN = imgEofN;
            imgPlotUofN = imgUofN;
            tablNeuronScat.Clear();
            tabl.ForEach(x => {
                tablNeuronScat.Add(new PracticeCalcElement(x));
            });
            labPractCompl = true;
        }

        public PrintDataModel()
        {
            labPractCompl = false;
            tablNeuronScat = new List<IPracticeCalcElement>();
        }
    }
}
