﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeutronsScattering.Model
{
    public interface IChemElement{
        string Name { get; }
        double MassNumber { get; }
        double Eps { get; }
        double RelativeSize { get; }
        string ToString();
    }

    public struct ChemElement: IChemElement
    {
        public string Name { private set; get; }
        public double MassNumber { private set; get; }
        public double Eps { private set; get; }
        public double RelativeSize { get; private set; }

        public ChemElement(string _name, double _mass, double relativeSize)
            : this()
        {
            Name = _name;
            MassNumber = _mass;
            Eps = Math.Pow((this.MassNumber - 1) / (this.MassNumber + 1), 2);
            RelativeSize = relativeSize;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public static class Chemestry
    {
        public static ChemElement H1 = new ChemElement("H(1)", 1d, 10d);
        public static ChemElement H2 = new ChemElement("H(2)", 2d, 20d);
        public static ChemElement He4 = new ChemElement("He(4)", 4.0, 20d);
        public static ChemElement Be9 = new ChemElement("Be(9)", 9.0, 30d);
        public static ChemElement C12 = new ChemElement("C(12)", 12.0, 30d);
        public static ChemElement O16 = new ChemElement("O(16)", 16.0, 40d);
        public static ChemElement U238 = new ChemElement("U(238)", 238.0, 50d);

        public static List<IChemElement> ElementsList = new List<IChemElement>(){ H1, H2, He4, Be9, C12, O16, U238 };
    }
}
