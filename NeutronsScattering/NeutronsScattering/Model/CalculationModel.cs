﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeutronsScattering.Model
{
    public interface ICalculationModel
    {
        [Obsolete("Планируется убрать данный hanler в будующем.")]
        event EventHandler ModelUpdated;
        IChemElement UsingElement { get; }

        double Theta { get; }

        double Psi { get; }

        double CosTheta { get; }

        double CosPsi { get; }

        /// <summary>Начальная энергия</summary>
        double InitialEnergy { get; }
        
        /// <summary>Конечная энергия</summary>
        double FinalEnergy { get; }
        
        /// <summary>Энергия до столкновения</summary>
        double StartEnergy { get; }

        /// <summary>Энергия после столкновения</summary>
        double EndEnergy { get; }

        double RelativeEnergyLoss { get; }

        double Xi { get; }

        double U { get; }
        
        void NextValues();

        bool NextValues(double rndNum);

        [Obsolete("Скорее всего метод будет удален")]
        void RestartCalc();
        
        void ChangeInitialValues(IChemElement element, double initial, double final);
    }


    public class CalculationModel : ICalculationModel
    {
        private Random _randGen;
        public IChemElement UsingElement { get; private set; }
        public double Theta { get; private set; }
        public double Psi { get; private set; }
        public double CosTheta { get; private set; }
        public double CosPsi { get; private set; }
        public double InitialEnergy { get; private set; }  //E0
        public double FinalEnergy { get; private set; }    //Et, скорее всего данное значение не понадобится
        public double StartEnergy { get; private set; }    //E0 в итерации
        public double EndEnergy { get; private set; }      //E1 в итерации
        public double RelativeEnergyLoss { get; private set; }
        public double Xi { get; private set; }
        public double U { get; private set; }

        public event EventHandler ModelUpdated = delegate { };

        public CalculationModel()
        {
            ChangeInitialValues(Chemestry.H1, 1000000, 100);
            _randGen = new Random();
        }
        public CalculationModel(IChemElement usElement, double initVal, double finalVal)
        {
            ChangeInitialValues(usElement, initVal, finalVal);
            _randGen = new Random();
        }

        [Obsolete("Скорее всего метод будет удален")]
        public void RestartCalc()
        {
            StartEnergy = InitialEnergy;
            EndEnergy = InitialEnergy;
            NextValues();
        }

        public bool NextValues(double rndNum)
        {
            StartEnergy = EndEnergy;
            Theta = rndNum * 2 * Math.PI;
            CosTheta = Math.Cos(Theta);
            CosPsi = (UsingElement.MassNumber * CosTheta + 1) /
                      (Math.Sqrt(Math.Pow(UsingElement.MassNumber, 2) + 2 * UsingElement.MassNumber * CosTheta + 1));

            Psi = Math.Acos(CosPsi);
            if (Theta > Math.PI)
                Psi = 2 * Math.PI - Psi;

            EndEnergy = StartEnergy / 2 * (1 + UsingElement.Eps + (1 - UsingElement.Eps) * CosTheta);
            RelativeEnergyLoss = (StartEnergy - EndEnergy) / EndEnergy;
            Xi = Math.Log(StartEnergy / EndEnergy);
            U = Math.Log(InitialEnergy / EndEnergy);

            return EndEnergy > FinalEnergy;
        }

        public void NextValues()
        {
            NextValues(_randGen.NextDouble());
            ModelUpdated(this, null);
        }

        public void ChangeInitialValues(IChemElement element, double initial, double final)
        {
            UsingElement = element;

            if (initial < final)
            {
                InitialEnergy = 0;
                FinalEnergy = 0;
                Console.WriteLine(@"Bad initial energy values.");
            }
            else
            {
                InitialEnergy = initial;
                FinalEnergy = final;
            }
            StartEnergy = InitialEnergy;
            EndEnergy = InitialEnergy;
        }
    }

}
