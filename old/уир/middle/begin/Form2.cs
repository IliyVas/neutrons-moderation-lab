﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using System.Drawing.Drawing2D;

namespace begin
{
    public partial class Form2 : Form
    {

        public Form2()
        {
            InitializeComponent();
        }

        private void button111_Click(object sender, EventArgs e)
        {
            tabControl2.SelectedTab = tabControl2.TabPages["TabPage2"];
        }
        private void button12_Click(object sender, EventArgs e)
        {
            tabControl2.SelectedTab = tabControl2.TabPages["TabPage1"];
        }
        private void button13_Click(object sender, EventArgs e)
        {
            tabControl2.SelectedTab = tabControl2.TabPages["TabPage3"];
        }
        private void button14_Click(object sender, EventArgs e)
        {
            tabControl2.SelectedTab = tabControl2.TabPages["TabPage2"];
        } //обработчик кнопок перехода

        private float x = 0, y, q, w, r, u, i, aa;
        double z, v, c;
        int q1, q2;
        System.Drawing.Bitmap plane;
        int p12;
        double d1, d2, l, n1, y1, y11, e11, p41, e111, r11, r21, n10, y10, e110, p410, e1110, r110, r210;
        Rectangle rct, ww; // объявлене переменных

        private void Form2_Load(object sender, EventArgs e)
        {
            plane = new Bitmap("шар.png");
            rct.X = 30;
            rct.Y = 85;
            rct.Height = plane.Height / 2;
            rct.Width = plane.Width / 2;
            ww.X = 175;
            ww.Y = 70;
            comboBox1.SelectedItem = "H(1)";
            comboBox2.SelectedItem = "C(12)";
            g = 0;
            DrawGraph(); 
        }        //внесение начальных значений в переменные описывающие дизайн

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(plane, ww.X, ww.Y, 60, 40);
            System.Drawing.Graphics formGraphics = this.panel1.CreateGraphics();
            System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Color.Black);
            formGraphics.DrawLine(myPen, 316, 88, 320, 90);
            formGraphics.DrawLine(myPen, 320, 90, 100, 90);
            formGraphics.DrawLine(myPen, 316, 92, 320, 90);
        }       //изначальная графика

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }

        }       //запрет на ввод любых символ кроме цифр в графу Угол Q

        string[,] row = new string[100,9];
        int ii;

        private void button2_Click(object sender, EventArgs e)
        {
            if (!(textBox1.Text == ""))
            {
                int y;
                Refresh();
                y = Convert.ToInt32(textBox1.Text);     //перевод строки, введеную в Угол Q
                int yy = y;
                while (y >= 360)
                {
                    y = y - 360;
                    textBox1.Text = Convert.ToString(y);
                }       //уменьшение угла до <360
                System.Drawing.Graphics formGraphics = this.panel1.CreateGraphics();
                System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Color.Black);

                double o, n, m;
                o = (y * Math.PI * 100) / 180;
                o = -o / 100;
                n = Math.Cos(o);
                m = Math.Sin(o);        //подсчет параметров угла
                
                formGraphics.DrawLine(myPen, 205.0f, 90.0f, 205 + (float)n * 100, 90 + (float)m * 100);
                formGraphics.DrawImage(plane, 200 + (float)n * 100, 85 + (float)m * 100, 18, 14);
                Pen Pen2 = new Pen(Color.Black, 3);
                formGraphics.DrawArc(Pen2, 182.0f, 67.0f, 46.0f, 46.0f, 0.0f, -y);      //прорисовка графики с заданным углом

                if (comboBox1.SelectedItem.ToString() == "H(1)")
                {
                    d1 = 0;
                    d2 = 2;
                    l = 1;
                }
                else
                    if (comboBox1.SelectedItem.ToString() == "H(2)")
                    {
                        l = 2;
                        d1 = 1;
                        d2 = 3;

                    }
                    else
                        if (comboBox1.SelectedItem.ToString() == "He(4)")
                        {
                            l = 4;
                            d1 = 3;
                            d2 = 5;
                        }
                        else
                            if (comboBox1.SelectedItem.ToString() == "Be(9)")
                            {
                                l = 9;
                                d1 = 8;
                                d2 = 10;
                            }
                            else
                                if (comboBox1.SelectedItem.ToString() == "C(12)")
                                {
                                    l = 12;
                                    d1 = 11;
                                    d2 = 13;
                                }
                                else
                                    if (comboBox1.SelectedItem.ToString() == "O(16)")
                                    {
                                        l = 16;
                                        d1 = 15;
                                        d2 = 17;
                                    }
                                    else
                                        if (comboBox1.SelectedItem.ToString() == "U(238)")
                                        {
                                            l = 238;
                                            d1 = 237;
                                            d2 = 239;
                                        }       //задание параметров для различных нуклидов

                double p1 = l * Math.Cos(y) + 1;
                double p2 = l * l + 2 * l * Math.Cos(y) + 1;
                double p3 = Math.Sqrt(p2);
                if (p1 < 0)
                {
                    p1 = -p1;
                }
                
                double p4 = p1 / p3;
                double eps1 = (double)d1 / d2;
                eps1 = eps1 * eps1;
                double e1 = 1000000 * ((1 + eps1) + (1 - eps1) * Math.Cos(y));
                double r1 = (2000000 / e1);
                double r0 = (2000000 - e1) / 2000000;
                double r2 = Math.Log(2000000 / e1);

                ii++;
                n = Math.Round(n, 2);
                p4 = Math.Round(p4, 2);
                e1 = Math.Round(e1, 0);
                r0 = Math.Round(r0, 2);
                r2 = Math.Round(r2, 4);      //подсчет необходимых значений и их округление

                row[ii,1] = ( ii.ToString() );
                row[ii,2] = ( comboBox1.SelectedItem.ToString() );
                row[ii,3] = ( y.ToString() );
                row[ii,4] = ( n.ToString() );
                row[ii,5] = ( p4.ToString() );
                row[ii,6] = ( e1.ToString() );
                row[ii,7] = ( r0.ToString() );
                row[ii,8] = ( r2.ToString() ); //добавлние значений в таблицу

                y1 = y1 + y;
                n1 = n1 + n;
                p41 = p41 + p4;
                e111 = e111 + e1;
                r11 = r11 + r0;
                r21 = r21 + r2;

                y10 = y1 / ii;
                n10 = n1 / ii;
                p410 = p41 / ii;
                e1110 = e111 / ii;
                r110 = r11 / ii;
                r210 = r21 / ii;

                y10 = Math.Round(y10, 2);
                n10 = Math.Round(n10, 2);
                p410 = Math.Round(p410, 2);
                e110 = Math.Round(e110, 0);
                r110 = Math.Round(r110, 2);
                r210 = Math.Round(r210, 4);

                row[ii+1,1] =  "Среднее";
                row[ii+1,2] =  "";
                row[ii+1,3] =  y10.ToString();
                row[ii+1,4] =  n10.ToString();
                row[ii+1,5] =  p410.ToString() ;
                row[ii+1,6] =  e1110.ToString() ;
                row[ii+1,7] =  r110.ToString() ;
                row[ii + 1, 8] = r210.ToString(); // Подсчет средних значений и занесения их в таблицу

                if (button5.Text == "Оставить только среднее")
                {
                    if (!(ii == 1))
                    {
                        dataGridView1.Rows.RemoveAt(ii - 1);
                    }
                    dataGridView1.Rows.Add(row[ii, 1], row[ii, 2], row[ii, 3], row[ii, 4], row[ii, 5], row[ii, 6], row[ii, 7], row[ii, 8]);
                    dataGridView1.Rows.Add(row[ii + 1, 1], row[ii + 1, 2], row[ii + 1, 3], row[ii + 1, 4], row[ii + 1, 5], row[ii + 1, 6], row[ii + 1, 7], row[ii + 1, 8]);
                } // изначальный вывод таблицы если ни разу не нажата кнопка "Оставить только среднее"
                else
                {
                    if (!(ii ==1))
                    {
                    dataGridView1.Rows.RemoveAt(0);
                    }
                    dataGridView1.Rows.Add(row[ii + 1, 1], row[ii + 1, 2], row[ii + 1, 3], row[ii + 1, 4], row[ii + 1, 5], row[ii + 1, 6], row[ii + 1, 7], row[ii + 1, 8]);
                }
            }
            else
            {
                MessageBox.Show("Введите число.");
            } // сообщение при попытке выполнения программы без ввода значения
        }

        private void button1_Click(object sender, EventArgs e)
        {          
                Random rand = new Random();
                int z;
                z = rand.Next(0, 360);
                textBox1.Text = z.ToString();
                button2.PerformClick();            
        }       //обработчик события при нажатии кнопки "Случайное"

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            Rectangle zag = new Rectangle(10, 0, 290, 30);
            System.Drawing.Font zam = new System.Drawing.Font("Lucida Console", 16F);
            e.Graphics.DrawString("ТАБЛИЦА РЕЗУЛЬТАТОВ", zam, Brushes.Black, zag);
        }       //написание словосочетания "ТАБЛИЦА РЕЗУЛЬТАТОВ"

        private void button3_Click(object sender, EventArgs e)
        {
            ii = 0;
            dataGridView1.Rows.Clear();
            y1 = 0;
            n1 = 0;
            p41 = 0;
            e111 = 0;
            r11 = 0;
            r21 = 0;
        }           //Обнуление среднего значения и таблицы при нажатии кнопки "Стереть все"

        private void button5_Click(object sender, EventArgs e)
        {
            if (button5.Text == "Показать значения")
            {
                button5.Text = "Оставить только среднее";
                 dataGridView1.Rows.RemoveAt(0);
                 for (int c = 1; c <= ii; c++)
                 {
                     dataGridView1.Rows.Add(row[c, 1], row[c, 2], row[c, 3], row[c, 4], row[c, 5], row[c, 6], row[c, 7], row[c, 8]);
                 }
                 dataGridView1.Rows.Add(row[ii + 1, 1], row[ii + 1, 2], row[ii + 1, 3], row[ii + 1, 4], row[ii + 1, 5], row[ii + 1, 6], row[ii + 1, 7], row[ii + 1, 8]);
            }
            else 
            {
                button5.Text = "Показать значения";
                for (int c=1; c <= ii; c++)
                {
                    dataGridView1.Rows.RemoveAt(0);
                }
            } // Скрывание/возврат таблицы
        }

        int g;
        double g1; // добавление новых переменных

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
        } // ограничения на ввод только чисел

        private void DrawGraph()
        {
           // Получим панель для рисования
           GraphPane pane = zedGraph.GraphPane;

           // Изменим текст по оси X и Y
           pane.XAxis.Title.Text = "Количество судеб";
           pane.YAxis.Title.Text = "Средняя логарифмеческая потеря энергии ξ";
           // Изменим текст заголовка графика
            pane.Title.Text = "";
            // Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
            pane.CurveList.Clear();
            AddCurve2(pane);
            // Создадим список точек
            PointPairList list = new PointPairList();

            int xmin = 0;
            int xmax = g;

            // Заполняем список точек
            for (int x = xmin; x <= xmax; x += 1)
            {
                // добавим в список точку
                list.Add(x, ar1[x]);
            }

            // Создадим кривую с названием "Практическое ξ", 
            // которая будет рисоваться синим цветом
            // Опорные точки выделяться не будут 
            LineItem myCurve = pane.AddCurve("Практическое ξ", list, Color.Blue, SymbolType.None);

            pane.XAxis.Scale.Min = 0;
            pane.XAxis.Scale.MaxAuto = true;
            pane.YAxis.Scale.Min = 0;
            pane.YAxis.Scale.MaxAuto = true;
            // Устанавливаем интересующий нас интервал по оси Х и Y
       
            // Обновляем данные об осях при изменении масштаба 
            zedGraph.AxisChange();
            
            // Обновляем график
            zedGraph.Invalidate();
        }

        private void AddCurve2(GraphPane pane)
        {
            PointPairList list = new PointPairList();
            double vv;
            double xmin = 0;
            int xmax = g;
            if (l == 1)
            {
                vv = 1;
            }
            else if (l == 2)
            {
                vv = 0.725;
            }
            else if (l == 4)
            {
                vv = 0.425;
            }
            else if (l == 9)
            {
                vv = 0.209;
            }
            else if (l == 12)
            {
                vv = 0.158;
            }
            else if (l == 16)
            {
                vv = 0.12;
            }
            else
            {
                vv = 0.0084;
            }
            for (double x = xmin; x <= xmax; x += 1)
            {
                list.Add(x, vv);
            } // Теоретические значения для разных нуклидов
            
            // Создадим кривую с названием "Теоретическое ξ", 
            // которая будет рисоваться коричневым цветом опорные точки выделяться не будут 
            LineItem myCurve = pane.AddCurve("Теоретическое ξ", list, Color.Brown, SymbolType.None);

            // Используем предустановленный стиль, рисующий кривую в виде штрихпунктирной линии.
            myCurve.Line.Style = DashStyle.DashDot;

            // Укажем, что график должен быть сглажен
            myCurve.Line.IsSmooth = true;
        }

        double[] ar1 = new double[10000];
        private void button4_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem.ToString() == "H(1)")
            {
                d1 = 0;
                d2 = 2;
                l = 1;
            }
            else
                if (comboBox2.SelectedItem.ToString() == "H(2)")
                {
                    l = 2;
                    d1 = 1;
                    d2 = 3;
                }
                else
                    if (comboBox2.SelectedItem.ToString() == "He(4)")
                    {
                        l = 4;
                        d1 = 3;
                        d2 = 5;
                    }
                    else
                        if (comboBox2.SelectedItem.ToString() == "Be(9)")
                        {
                            l = 9;
                            d1 = 8;
                            d2 = 10;
                        }
                        else
                            if (comboBox2.SelectedItem.ToString() == "C(12)")
                            {
                                l = 12;
                                d1 = 11;
                                d2 = 13;
                            }
                            else
                                if (comboBox2.SelectedItem.ToString() == "O(16)")
                                {
                                    l = 16;
                                    d1 = 15;
                                    d2 = 17;
                                }
                                else
                                    if (comboBox2.SelectedItem.ToString() == "U(238)")
                                    {
                                        l = 238;
                                        d1 = 237;
                                        d2 = 239;
                                    }       //задание параметров для различных нуклидов
            Refresh();
            double kk = 0;

            if (!(textBox2.Text == ""))
            {

                g = Convert.ToInt32(textBox2.Text);
                Random rand = new Random();
                double z;

                for (int j = 1; j <= g; j++)
                {
                    z = rand.Next(0, 360);
                    z = z - 180;
                    if (( (z == 180) || ( z == -180 ) ) & (comboBox2.SelectedItem.ToString() == "H(1)"))
                    {
                        z = z - 1;
                    }
                                       
                    z = z * Math.PI;
                    z = z / 180;

                    double eps1 = (double)d1 / d2;
                    eps1 = eps1 * eps1;
                    double e1 = 1000000 * ((1 + eps1) + (1 - eps1) * Math.Cos(z));
                    double r2 = Math.Log(2000000 / e1); // создание и расчет одной произвольной судьбы
                    
                    r2 = Math.Round(r2, 4);
                    kk = kk + r2;
                    r2 = kk / j;

                    ar1[j] = r2; // занесение в массив и усреднее учитывая предыдущие значения массива.
                }
                DrawGraph(); // после внесения всех значения в массив вызывается процедура прорисовки
            }
            else
            {
                MessageBox.Show("Введите число.");
            }  // сообщение при попытке выполнения программы без ввода значения
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DrawGraph(); // обновление графика при нажатии кнопки Первоначальный масштаб
        }
    }
}