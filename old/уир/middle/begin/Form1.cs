﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace begin
{

    public partial class Form1 : Form
    {
        private float x = 0, y, q, w, r, u, i, aa;
        double z, v, c;
        int q1, q2;        
        System.Drawing.Bitmap plane;
        int p12;
        Rectangle rct, ww; // Обьявление перменных

        public Form1(string d)
        { 
            InitializeComponent();            
            if (d == "1")
            {
                comboBox1.Text= "Замедление нейтронов";
            } // внесение значение в выпадающем списке(левый верхний угол) по умолчанию "Замедление нейтронов"
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            plane = new Bitmap("шар.png");
            rct.X = 30;
            rct.Y = 85;
            rct.Height = plane.Height / 2;
            rct.Width = plane.Width / 2;
            ww.X = 175;
            ww.Y = 70;
        } // Загрузка некоторых обьектов интерфейса

        Form3 f8;
        private void button1_Click(object sender, EventArgs e)
        {

            for (int o = 0; o <= 10; o++)
            {
            Thread.Sleep(30);
            this.Opacity = this.Opacity - 0.1;
            }
            Application.Exit();
        } // Плавное выключение программы
        
         void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {                             
            if (comboBox1.SelectedItem.ToString() == "Замедление нейтронов")
            {
                tabControl1.Visible = true;
                button4.Visible = true;
                button9.Visible = true;
                timer2.Enabled = true;
                timer2.Interval = 1;  
                panel1.Controls.Add(tabControl1);
                label2.Text = "";
                label3.Text = "";
                label4.Text = "";
                label5.Text = "";                    
                }
         } //  добавление элементов интерфейса на соответствующую подвкладку при выборе "Замедление нейтронов"

        private void button2_Click(object sender, EventArgs e)
        { tabControl1.SelectedTab = tabControl1.TabPages["TabPage2"]; }

        private void button3_Click(object sender, EventArgs e)
        { tabControl1.SelectedTab = tabControl1.TabPages["TabPage3"]; }

        private void button5_Click(object sender, EventArgs e)
        { tabControl1.SelectedTab = tabControl1.TabPages["TabPage1"]; }

        private void button6_Click(object sender, EventArgs e)
        { tabControl1.SelectedTab = tabControl1.TabPages["TabPage2"]; }
        // Обработчик переходов

        Form2 f3;
        private void button4_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Замедление нейтронов")
            {               
                if (f3 == null || f3.IsDisposed)
                    {
                           f3 = new Form2();
                           f3.Show();
                    }                  
            }
        } // запуск программы практики при нажати соответсвующей кнопки, если она не запущена         
            
        private void panel3_Paint(object sender, PaintEventArgs e)
        {             
            Rectangle zag = new Rectangle(210, 10, 2000, 390);
            Rectangle re = new Rectangle(330, 50, 230, 390);
         
            System.Drawing.Font z = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            System.Drawing.Font za = new System.Drawing.Font("Segoe UI", 14F);            
            e.Graphics.DrawString("Замедление нейтронов -", za, Brushes.Black, re);
            e.Graphics.DrawString("\n\nпроцесс снижения их энергии в результате рассеяния.\n\n"+
                "Рассмотрим акт рассеяния нейтрона на ядре с массовым числом А(масса нейтрона принимается за 1).", z, Brushes.Black, re);

        } //  Добавление текста на подвкладку

        private void button7_Click(object sender, EventArgs e)
        {
            timer2.Stop();
        }
        private void button8_Click(object sender, EventArgs e)
        {
            timer2.Start();
        } // Обработчик кнопок "Стоп" и "Запуск"

        private void timer2_Tick_1(object sender, EventArgs e)
        {
          if (x < 176)
            {
                x++;
                rct.X++; // перемещение картинки нейтрона, если она не дошла до необходимого тика таймера
            }
            else
            {
                u++;
            }
          panel2.Invalidate(rct); // обновление панели с анимацией
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
             e.Graphics.DrawImage(plane, ww.X, ww.Y, 60, 40);
            if (x < 170)
                {
                    e.Graphics.DrawImage(plane, rct.X, rct.Y, 18, 14);
                    e.Graphics.DrawImage(plane, ww.X, ww.Y, 60, 40);
                }            
            System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Color.Black);
            System.Drawing.Pen myPen1 = new System.Drawing.Pen(System.Drawing.Color.Black);
            System.Drawing.Pen myPen2 = new System.Drawing.Pen(System.Drawing.Color.Black);
            System.Drawing.Graphics formGraphics = this.panel2.CreateGraphics();
            Pen Pen2 = new Pen(Color.Black, 3);
            if (x < 175)
            {
                formGraphics.DrawLine(myPen, 30 + x, 90, 35, 90);
                label2.Text = "";
                label3.Text = "";
                label4.Text = "";
                label5.Text = "";
            }
            else if (x == 175)
            {
                formGraphics.DrawLine(myPen, 300, 90, 35, 90);
                Random rand = new Random();
                z = rand.NextDouble();
                z = z - 0.5;
                y = (float)(z * 2 * Math.PI);
                q = (float)Math.Cos(y);
                w = (float)Math.Sin(y);
                r = (float)((y * 180) / Math.PI); //  подсчет значений для произвольного угла
                formGraphics.DrawArc(Pen2, 182.0f, 67.0f, 46.0f, 46.0f, 0.0f, r); // прорисовка дуги угла
                if (u < 40)
                {
                    u = 40;
                }
                formGraphics.DrawLine(myPen2, 205 + q * 40, 90 + w * 40, 205, 90); // прорисовка траектории движения нейтрона
                e.Graphics.DrawImage(plane, ww.X, ww.Y, 60, 40); //  прорисовка картинки нейтрона
            }
            else if (u < 90)
            {
                q1 = (int)Math.Round(q * u, 0);
                q2 = (int)Math.Round(w * u, 0);
                formGraphics.DrawLine(myPen, 296, 88, 300, 90);
                formGraphics.DrawLine(myPen, 300, 90, 35, 90);
                formGraphics.DrawLine(myPen, 296, 92, 300, 90); //  прорисовка оси и стрелки
                rct.X = 200 + q1;
                rct.Y = 85 + q2;
                e.Graphics.DrawImage(plane, 200+q1, 85+q2, 18, 14);
                formGraphics.DrawLine(myPen1, 205 + q * u, 90 + w * u, 205 + q * 20, 90 + w * 20);
                formGraphics.DrawArc(Pen2, 182.0f, 67.0f, 46.0f, 46.0f, 0.0f, r);
                formGraphics.DrawLine(myPen2, 205 + q * 40, 90 + w * 40, 205, 90);
                e.Graphics.DrawImage(plane, ww.X, ww.Y, 60, 40); //  повторная прорисовка
            }
            else if (u == 90)
            {
                e.Graphics.DrawImage(plane, 200 + q1, 85 + q2, 18, 14);
                formGraphics.DrawLine(myPen1, 205 + q * u, 90 + w * u, 205 + q * 20, 90 + w * 20);
                formGraphics.DrawArc(Pen2, 182.0f, 67.0f, 46.0f, 46.0f, 0.0f, r);
                formGraphics.DrawLine(myPen2, 205 + q * 40, 90 + w * 40, 205, 90);
                formGraphics.DrawLine(myPen, 296, 88, 300, 90);
                formGraphics.DrawLine(myPen, 300, 90, 35, 90);
                formGraphics.DrawLine(myPen, 296, 92, 300, 90); //  повторная прорисовка

                timer2.Interval = 5000; // остановка таймера на 5 секунд(нейтрон дошел до конца)
                double p1 = 12 * Math.Cos(r) + 1;
                double p2 = 145 + 24 * Math.Cos(r);
                double p3 = Math.Sqrt(p2);
                if (p1 < 0)
                {
                    p1 = -p1;
                }
                double p4 = p1 / p3;
                int d1 = 11;
                int d2 = 13;
                double eps1 = (double)d1 / d2;
                eps1 = eps1 * eps1;
                label3.Text = "Выбран элемент С => ε = " + eps1;
                label2.Text = "cos ψ = (Acosθ + 1)/√(A^2 +2Acosθ +1) = " + p4;
                if (r < 0)
                {
                    r = -r;
                    p12 = 1;
                }
                label1.Text = "Q = " + r.ToString() + "°";
                double e1 = 1000000 * ((1 + eps1) + (1 - eps1) * Math.Cos(r));
                label4.Text = "E1 = (E/2)*((1+ε) + (1-ε)cosθ) = " + e1;
                double r1 = Math.Log(2000000 / e1);
                label5.Text = "ξ = Ln(E/E1) = " + r1;
                if (p12 == 1)
                {
                    r = -r;
                }
            } // пропись получившихся значений на панель анимации
            else
            {
                timer2.Interval = 1;
                panel2.Invalidate();
                x = 0;
                u = 0;
                label1.Text = "Q = ";
                rct.X = 30;
                rct.Y = 85;
            } // после 5 секундной остановки обновление всех данных
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Замедление нейтронов")
            {
                System.Diagnostics.Process.Start("Теория.pdf");   
            }
        } // Запуск файла с теориейпри нажатии кнопки "Теория"

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        } // выход всей программы при закрытии данного окна
    }
}